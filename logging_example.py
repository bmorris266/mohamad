import time
import errno
import sys
import subprocess
from subprocess import PIPE
import tarfile
import argparse
import os
import paramiko
import apt
import netifaces

errlog = []

class Log(object):
	def logger(self, alert):		
		import logging
		logger = logging.getLogger('IMPI')
		hdlr = logging.FileHandler('ipmi.log')
		formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s', '%m-%d-%Y %H:%M:%S')
		hdlr.setFormatter(formatter)
		logger.addHandler(hdlr)
		logger.setLevel(logging.WARNING)
		logger.error(alert)
		
class OpenVPN(object):
	def __init__(self, asset):
		self.asset = asset
		self.oc = 'OC' + str(self.asset)
		self.dir = '/etc/openvpn/keys'
		self.tar = self.dir + '/' + self.oc + '.tar'
		self.old_crt = '/etc/openvpn/keys/' + self.oc + '.crt'
		self.old_key = '/etc/openvpn/keys/' + self.oc + '.key'
		self.new_crt = '/etc/openvpn/keys/cert.crt'
		self.new_key = '/etc/openvpn/keys/key.key'
		self.oldca = '/etc/openvpn/keys/ca2.crt'
		self.newca = '/etc/openvpn/keys/ca.crt'
		self.old_client = '/etc/openvpn/keys/client.conf'
		self.new_client = '/etc/openvpn/client.conf'
	
	def installOpenvpn(self):
		try:
                	cache = apt.Cache()
                	if cache['openvpn'].is_installed:
                        	pass
                	else:
                        	pkg = cache['openvpn']
                        	pkg.mark_install()
                        	cache.commit()

                	if not os.path.exists(self.dir):
                        	os.makedirs(self.dir)
			return 0
        	except:
                	errlog.append('FAILED-Openvpn is not installed')
                	return 1

	def sftp(self):
        	source = '/home/user/oneconnxt/keys/' + self.oc + '.tar'
        	dest = '/etc/openvpn/keys/' + self.oc + '.tar'
        	ssh = paramiko.SSHClient()
        	ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        	ssh.connect('ftp2.oneconnxt.com', username='user', password='Prvich33#', timeout=0.5)
        	sftp = ssh.open_sftp()
        	try:
                	sftp.stat(source)
        	except IOError, e:
			if e.errno == errno.ENOENT:
                		errlog.append('FAILED-Failed to get key from SFTP')
                		return 1
		sftp.get(source, dest)
        	sftp.close()
        	ssh.close()
		return 0

	def configureOpenvpn(self):
        	old_crt = '/etc/openvpn/keys/' + self.oc + '.crt'
        	old_key = '/etc/openvpn/keys/' + self.oc + '.key'
        	new_crt = '/etc/openvpn/keys/cert.crt'
        	new_key = '/etc/openvpn/keys/key.key'
        	oldca = '/etc/openvpn/keys/ca2.crt'
        	newca = '/etc/openvpn/keys/ca.crt'
        	old_client = '/etc/openvpn/keys/client.conf'
        	new_client = '/etc/openvpn/client.conf'
        	try:
                	tarfil = tarfile.open(self.tar)
                	tarfil.extractall(self.dir)
                	tarfil.close()
        	except:
                	errlog.append('FAILED-Failed to untar key files')
                	return 1
        	try:
                	os.renames(self.old_crt, self.new_crt)
                	os.renames(self.old_key, self.new_key)
                	os.renames(self.oldca, self.newca)
                	os.renames(self.old_client, self.new_client)
                	proc = subprocess.Popen('/etc/init.d/openvpn restart', shell=True)
                	#proc.wait()
		except:
                	errlog.append('FAILED-Failed to restart Openvpn')
                	return 1

		time.sleep(5)

		try:
                        addrs = netifaces.ifaddresses('tun0')
                        if netifaces.AF_INET in addrs:
                                return 0
                        else:
                                errlog.append('FAILED-Interface tun0 not up')
                                return 1
		except:
			errlog.append('FAILED-Interface tun0 not up')
                        return 1


def ipmiSetup():
	addrs = netifaces.ifaddresses('tun0')
	ip = addrs[netifaces.AF_INET][0]['addr'].split('.')
	ip[1] = 9
	ip[2] = 0
	ipmiBIOSip = '.'.join(str(v) for v in ip)
	ip[2] = 1
	ipmiSYSip = '.'.join(str(v) for v in ip)
	ipmicoms = ['ipmicfg-linux.x86_64 -dhcp off', 'ipmicfg-linux.x86_64 -m ' + ipmiBIOSip, 'ipmicfg-linux.x86_64 -k 255.255.254.0', 'ipmicfg-linux.x86_64 -user setpwd 2 C0smos33#']
	
	for item in ipmicoms:
		ipmicomProc = subprocess.Popen(item, shell=True, stdout=PIPE, stderr=PIPE)
		print ipmicomProc.stdout.readlines()
		ipmicomProc.wait()

	intf = netifaces.interfaces()
	temp = str(intf[1]).replace('eth0', 'eth0:1')
	command = 'ifup ' + temp
	inject = ['auto lo', 'iface lo inet loopback\n', 'auto ' + temp, 'iface ' + temp + ' inet static', 'address ' + ipmiSYSip, 'netmask 255.255.254.0']

	with open('/etc/network/interfaces', 'w') as f:
	        for line in inject:
	                f.write('%s\n' % line)

	script = ['#!/bin/bash', 'VIRT_INT=eth0:1', 'ifdown $VIRT_INT && ifup $VIRT_INT', 'exit 0']

	with open('/etc/NetworkManager/dispatcher.d/startvirt', 'w') as f:
		for line in script:
			f.write('%s\n' % line)
	os.chmod('/etc/NetworkManager/dispatcher.d/startvirt', 0755)	
	subprocess.Popen(command, shell=True)



comargs = argparse.ArgumentParser()
comargs.add_argument('-a', help='Name of the assets', type=int, required=True)
args = comargs.parse_args()
if args.a:
	vpn = OpenVPN(args.a)
	log = Log()
	if vpn.installOpenvpn() !=0:
		log.logger(errlog)
		sys.exit()
	elif vpn.sftp() != 0:
		log.logger(errlog)
		sys.exit()
	elif vpn.configureOpenvpn() != 0:
		log.logger(errlog)
		sys.exit()

	ipmiSetup() 
